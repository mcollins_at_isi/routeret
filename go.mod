module gitlab.com/mcollins_at_isi/routeret

go 1.13

require (
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pty v1.1.3 // indirect
	github.com/mattn/go-isatty v0.0.6 // indirect
	github.com/mattn/go-zglob v0.0.1 // indirect
	github.com/prometheus/common v0.2.0 // indirect
	github.com/prometheus/procfs v0.0.0-20190227231451-bbced9601137 // indirect
	gitlab.com/mergetb/engine v0.3.1
	gitlab.com/mergetb/xir v0.1.8
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421 // indirect
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
	google.golang.org/genproto v0.0.0-20190227213309-4f5b463f9597 // indirect
)
