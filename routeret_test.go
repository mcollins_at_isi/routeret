package routeret

import (
	"gitlab.com/mergetb/engine/pkg/realize"
	"gitlab.com/mergetb/xir/lang/go"
	"testing"
)

func TestBase(t *testing.T) {
	want := "routeret exists"
	got := RRIsHere()
	if got != want {
		t.Errorf("Well that failed with %q ", got)
	}
}

func TestLoading(t *testing.T) {
	//
	// Tests that we can load and find file contents
	//
	sourceFile := "./test/data/simple_topo.json"
	net, err := xir.FromFile(sourceFile)
	if err != nil {
		t.Errorf("Failed to open file: %q", err)
	} else {
		df := realize.DijkstraForest(net)
		err = DoBuildRoutes(net, df)
		if err != nil {
			t.Errorf("Failed to Buld routes: %q", err)
		}
	}
}

func TestStripNet(t *testing.T) {
	var start, want, got string
	start = "10.2.0.24/22"
	want = "10.2.0.24"
	got = StripNetFromIP(start)
	if got != want {
		t.Errorf("Failed to successfully strip net from IP: %s/%s", want, got)
	}
}

func TestMakeLinkLookupTable(t *testing.T) {
	// Tests the creation of the route lookup table.
	sourceFile := "./test/data/simple_topo.json"
	net, err := xir.FromFile(sourceFile)

	if err != nil {
		t.Errorf("Failed to open file: %q", err)
	}
	// Construct lookup Table
	lt := MakeLinkLookupTable(net)
	_, res := lt[LinkKey{"10.0.0.1", "10.0.0.5"}]
	if !res {
		t.Errorf("Failed to find an existing link 10.0.0,1->10.0.0.5")
	}
	_, res = lt[LinkKey{"10.0.0.1", "10.0.10.1"}]
	if res {
		t.Errorf("Found a nonexisting link 10.0.0.1->10.0.10.1")
	}
}

func TestBuildRoutesError(t *testing.T) {
	//
	// Tests that we can load and find file contents
	//
	sourceFile := "./test/data/simple_topo.json"
	net, err := xir.FromFile(sourceFile)
	if err != nil {
		t.Errorf("Failed to open file: %q", err)
	} else {
		df := realize.DijkstraForest(net)
		err = DoBuildRoutes(nil, df)
		if err == nil {
			t.Errorf("Quietly ran buildroutes with a null value, should have aborted with an error")
		}
	}

}
